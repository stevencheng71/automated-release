# [1.1.0](https://gitlab.com/stevencheng71/automated-release/compare/v1.0.0...v1.1.0) (2024-1-7)


### Features

* package.json add feature ([c3d8fcd](https://gitlab.com/stevencheng71/automated-release/commit/c3d8fcd41d76565fb11b2541b5e762f50adff989))

# 1.0.0 (2024-01-07)


### Features

* add husky and commitlint ([fb3a6ab](https://gitlab.com/stevencheng71/automated-release/commit/fb3a6ab1c0ea318afababb2ce091acaaeef4c41a))
